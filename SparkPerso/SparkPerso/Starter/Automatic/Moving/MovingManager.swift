//
//  MovingManager.swift
//  SparkPerso
//
//  Created by AL on 11/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import DJISDK

// Un manageur de Mouvement/Déplacement
// Qui liste les mouvements et permet de les afficher dans la console
class MovingManager {
    
    static let instance = MovingManager()
    var mouvements = [Movement]()
    
    func restart() {
        DirectionSequence.instance.content.removeAll()
        mouvements = []
    }
    
    func appendMouvement(mouvement:Movement) {
        DirectionSequence.instance.content.append(mouvement.description())
        mouvements.append(mouvement)
    }
    
    func play() {
        
        print("*** Playing sequence ***")
        executeMove()
        
    }
    
    func executeMove() {
        if let move = mouvements.first {
            
            // Ici Envoyer la direction au drone == Remplir les sticks
            sendCommand(move)
            Timer.scheduledTimer(withTimeInterval: TimeInterval(move.duration), repeats: false) { (t) in
                // Code exécuté après move.duration seconds
                self.mouvements.remove(at: 0)
                self.executeMove()
            }
            
        }else{
            // Envoyer stop au drone == Remplir les sticks avec des 0
            resetStick();
        }
    }
    
    
//    Movement(value: -commonValue, type: .down)
    func sendCommand(_ movement:Movement) {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            resetStick()
            print(movement.description())
            switch movement.direction {
                case .forward,.back:
                    mySpark.mobileRemoteController?.rightStickVertical = Float(movement.direction.value().y)
                case .forwardLeft,.forwardRight:
                    mySpark.mobileRemoteController?.rightStickVertical = Float(movement.direction.value().y)
                    mySpark.mobileRemoteController?.rightStickHorizontal = Float(movement.direction.value().x)
                case .gimbalLookUp:
                    GimbalManager.instance.moveGimbal(pitchValue: 0 as NSNumber, time: 0.5)
                    Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (t) in
                       CameraManager.instance.takePicture()
                    }
                
                case .gimbalLookDown:
                    GimbalManager.instance.moveGimbal(pitchValue: -80 as NSNumber, time: 0.5)
                    Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (t) in
                        CameraManager.instance.takePicture()
                    }
                default:
                    print("nothing found as command")
            }
        }
    }
    
    func resetStick(){
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            print("reset sticks")
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
        }else{
            print("mySpark not found")
        }
    }
    
}
