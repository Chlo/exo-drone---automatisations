//
//  GimbalManager.swift
//  SparkPerso
//
//  Created by AL on 11/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import DJISDK

class  GimbalManager: NSObject, DJIGimbalDelegate {
    
    static let instance = GimbalManager()
    var mouvements = [GimbalMovement]()
    
    
    func restart() {
        DirectionSequence.instance.content.removeAll()
        mouvements = []
    }
    
    func appendMouvement(mouvement:GimbalMovement) {
        DirectionSequence.instance.content.append(mouvement.description())
        mouvements.append(mouvement)
    }
    
    func play() {
        
        print("*** Playing sequence ***")
        executeMove()
        
    }
    
    func executeMove() {
        if let move = mouvements.first {
            
            // Ici Envoyer la direction au drone == Remplir les sticks
            print(move.description())
            
            Timer.scheduledTimer(withTimeInterval: TimeInterval(move.duration), repeats: false) { (t) in
                // Code exécuté après move.duration seconds
                self.mouvements.remove(at: 0)
                self.executeMove()
            }
            
        }else{
            // Envoyer stop au drone == Remplir les sticks avec des 0
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                mySpark.mobileRemoteController?.leftStickVertical = 0.0
                mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickVertical = 0.0
            }
            print("SEND STOP!!!!!")
        }
    }
    
    func moveGimbal(pitchValue : NSNumber, time : Float){

        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            
            if let myGimbal = mySpark.gimbal{

                myGimbal.delegate = self
                
                let rotation = DJIGimbalRotation.init(pitchValue: pitchValue, rollValue: nil, yawValue: nil, time: TimeInterval(time), mode: DJIGimbalRotationMode.absoluteAngle)
                
                myGimbal.rotate(with: rotation) { (err) in
                    print(err ?? "No problemo")
                }
                
            }
        }
    }
    
}

