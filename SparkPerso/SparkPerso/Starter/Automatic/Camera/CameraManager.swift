//
//  GimbalManager.swift
//  SparkPerso
//
//  Created by AL on 11/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import DJISDK
import VideoPreviewer
import ImageDetect

class  CameraManager: NSObject {
    
    static let instance = CameraManager()
    
    var imageView : UIImageView? = nil
    let prev1 = VideoPreviewer()
    var videoView: UIView? = nil
    
    func initManager(image : UIImageView, video : UIView){
        imageView = image
        videoView = video
    }
    
    func setupView(){
        if let _ = DJISDKManager.product() {
            if let camera = self.getCamera(){
                camera.delegate = self
                self.setupVideoPreview()
            }
            
        }
    }
    
    func getCamera() -> DJICamera? {
        // Check if it's an aircraft
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            return mySpark.camera
        }
        
        return nil
    }
    
    
    
    func setupVideoPreview() {
        
        prev1?.setView(self.videoView)
        //VideoPreviewer.instance().setView(self.cameraView)
        if let _ = DJISDKManager.product(){
            DJISDKManager.videoFeeder()?.primaryVideoFeed.add(self, with: nil)
        }
        prev1?.start()
        //VideoPreviewer.instance().start()
        
    }
    
    func resetVideoPreview() {
        prev1?.unSetView()
        DJISDKManager.videoFeeder()?.primaryVideoFeed.remove(self)
        
    }
    
    func takePicture(){
        print("take a pircture")
        //Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
            self.prev1?.snapshotThumnnail { (image) in
                if let img = image {
                    self.imageView?.image = img
                    if let res = ImageRecognition.shared.predictUsingCoreML(image: img){
                        print(res)
                    }
                    
                    

//                    print(img.size)
//
//                    img.detector.crop(type: .face) { [weak self] result in
//                        switch result {
//                        case .success(let croppedImages):
//                            // When the `Vision` successfully find type of object you set and successfuly crops it.
//                            print("Found")
//                            // Resize it and put it in a neural network! :)
//                            self?.imageView?.image = croppedImages[0]
//                            return
//                        case .notFound:
//                            // When the image doesn't contain any type of object you did set, `result` will be `.notFound`.
//                            print("Not Found")
//                            return
//                        case .failure(let error):
//                            // When the any error occured, `result` will be `failure`.
//                            print(error.localizedDescription)
//                            return
//                        }
                    //}
                    
                    
                }
            }
        }
    }
    
    


extension CameraManager:DJIVideoFeedListener {
    func videoFeed(_ videoFeed: DJIVideoFeed, didUpdateVideoData videoData: Data) {
        
        videoData.withUnsafeBytes { (bytes:UnsafePointer<UInt8>) in
            prev1?.push(UnsafeMutablePointer(mutating: bytes), length: Int32(videoData.count))
            
        }
        
    }
    
    
}

extension CameraManager:DJISDKManagerDelegate {
    func appRegisteredWithError(_ error: Error?) {
        
    }
    
    
}

extension CameraManager:DJICameraDelegate {
    
}

