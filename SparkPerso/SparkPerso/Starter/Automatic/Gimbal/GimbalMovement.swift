//
//  Mouvement.swift
//  SparkPerso
//
//  Created by AL on 11/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

struct GimbalMovement {
    var direction:Direction
    var duration:CGFloat
    
    enum Direction:String,CaseIterable {
        case up,down
        
        func value() -> CGPoint {
            switch self {
            case .up: return CGPoint(x: 0, y: 1)
            case .down: return CGPoint(x: 0, y: -1)
            }
        }
    }
    
    func description() -> String {
        return "\(direction.rawValue) during \(duration)s"
    }
}

