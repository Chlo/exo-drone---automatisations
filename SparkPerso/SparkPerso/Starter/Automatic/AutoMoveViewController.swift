//
//  AutoMoveViewController.swift
//  SparkPerso
//
//  Created by AL on 11/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import DJISDK
import VideoPreviewer
import ImageDetect

// Créé une suite de mouvement (séquence)
// Puis "joue" cette séquence
// La durée est respectée lors de l'affichage dans la console

class AutoMoveViewController: UIViewController {
    
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    let isTesting = false

    override func viewDidLoad() {
        
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        CameraManager.instance.initManager(image : imageView, video: videoView)
        CameraManager.instance.setupView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func stopButton(_ sender: Any) {
        
        if isTesting{
            print("Stop button clicked")
        } else  {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                print("reset sticks")
                mySpark.mobileRemoteController?.leftStickVertical = 0.0
                mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickVertical = 0.0
                
                MovingManager.instance.restart()
            }else{
                print("mySpark not found")
            }
        }
        
    }

    @IBAction func takeOffButton(_ sender: Any) {
        
        if isTesting{
            print("take off button clicked")
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                if let flightController = mySpark.flightController {
                    flightController.startTakeoff(completion: { (err) in
                        print(err.debugDescription)
                    })
                }
            }
        }
        
    }
    
    @IBAction func automatisationButton(_ sender: Any) {
        
        
        if isTesting{
            print("auto button clicked")
        } else {
            MovingManager.instance.restart()
            MovingManager.instance.appendMouvement(mouvement: Movement(direction: .forward, duration: 3.0))
            MovingManager.instance.appendMouvement(mouvement: Movement(direction: .forwardRight, duration: 3.0))
            MovingManager.instance.appendMouvement(mouvement: Movement(direction: .gimbalLookDown, duration: 3.0))
            MovingManager.instance.appendMouvement(mouvement: Movement(direction: .forward, duration: 3.0))
            MovingManager.instance.appendMouvement(mouvement: Movement(direction: .forwardLeft, duration: 3.0))
            MovingManager.instance.appendMouvement(mouvement: Movement(direction: .gimbalLookUp, duration: 3.0))
            MovingManager.instance.play()
        }
        
    }
    
    @IBAction func landingButton(_ sender: Any) {
        
        if isTesting{
            print("landing button clicked")
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                if let flightController = mySpark.flightController {
                    flightController.startLanding(completion: { (err) in
                        print(err.debugDescription)
                    })
                }
            }
        }
    }
    
    

}


