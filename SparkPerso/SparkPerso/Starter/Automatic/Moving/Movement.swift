//
//  Mouvement.swift
//  SparkPerso
//
//  Created by AL on 11/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

struct Movement {
    var direction:Direction
    var duration:CGFloat
    
    
    enum Direction:String,CaseIterable {
        case forward,back,left,right,forwardRight,forwardLeft,backRight,backLeft,gimbalLookDown, gimbalLookUp
        
        func value() -> CGPoint {
            let speed:Double = 0.1
            
            switch self {
            case .forward: return CGPoint(x: 0 * speed, y: 1 * speed)
            case .back: return CGPoint(x: 0 * speed, y: -1 * speed)
            case .left: return CGPoint(x: -1 * speed, y: 0 * speed)
            case .right: return CGPoint(x: 1 * speed, y: 0 * speed)
            case .forwardRight: return CGPoint(x: 1 * speed, y: 1 * speed)
            case .forwardLeft: return CGPoint(x: -1 * speed, y: 1 * speed)
            case .backRight: return CGPoint(x: 1 * speed, y: -1 * speed)
            case .backLeft: return CGPoint(x: -1 * speed, y: -1 * speed)
            case .gimbalLookDown: return CGPoint(x: -90 * speed, y: 0 * speed)
            case .gimbalLookUp: return CGPoint(x: 0 * speed, y: 0 * speed )
            }
        }
    }
    
    func description() -> String {
        return "\(direction.rawValue) during \(duration)s"
    }
}

